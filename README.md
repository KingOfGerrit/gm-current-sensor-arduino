# GM current sensor example of usage by Arduino boards

This project is a template/example of how GM current sensor can be used (since 2005/2006 year)
by Arduino board to measure Amps.

<img src="gm-sensor-image-1.jpg"  width="30%" height="30%" alt="gm-sensor-image-1">
<img src="gm-sensor-image-2.jpg"  width="30%" height="30%" alt="gm-sensor-image-2">

## Installation

This project uses PlatformIO and CLion for development.

[PIO core](https://docs.platformio.org/en/latest/core/installation/index.html) or [Arduino IDE](https://www.arduino.cc/en/software) can be used to build.

To develop can be used any IDE that supports integration with [PIO](https://platformio.org/install/ide).
Or CLion with PlatformIO plugin.

## Usage

To flash Arduino board can be used two variants:
1. Download [Arduino IDE](https://www.arduino.cc/en/software). Download the latest [release]()/[artifacts](https://gitlab.com/KingOfGerrit/gm-current-sensor-arduino/-/artifacts) of Arduino IDE source files.
   Then you should open the `*.ino` file in Arduino IDE, choose a board, and flash it.

2. Download AVRDude or a similar tool that can flash AVR, and download the latest firmware from [release]()/[artifacts](https://gitlab.com/KingOfGerrit/gm-current-sensor-arduino/-/artifacts) for your board, and flash it.

### Default pinout

Pinout of GM current sensor:
```
C - Signal
B - Ground
A - 5 V
```
The default pin to connect the sensor is `D2`. Can be changed in code.

### Importing notes

After first start you should enter calibration values for you sensor:
- In the `main.cpp` there are couples values that should be changed depending on values that your sensor displays:
    - `sensorZeroAmpsRef`
    - `sensorMaxMappingValue`
- If you're using LCD to print the value from the sensor, you need to uncomment the `#define LCD_I2C` line in the `main.cpp`.
  Also, using an LCD I2C display is required to check the next steps:
    - The default address is `0x27`. It is defined in the `#define lcd_address 0x27` line in the `main.cpp`.
      Usually, the address of an I2C LCD is `0x27`, but if not, you should find this address. [How to find an I2C address.](https://learn.adafruit.com/scanning-i2c-addresses/arduino)
    - The default and minimal size of the LCD screen is 16x2. If you have a bigger LCD, change it.

## Notes

All libraries are included in the project to minimize steps to flash Arduino from Arduino IDE,
and also freeze versions.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Used code, libs, and info

- https://github.com/xkam1x/Arduino-PWM-Reader/tree/master
- https://github.com/ifail21/GM-Current-Sensor-Tester
- https://www.youtube.com/watch?v=mruUknOU7CI
- https://registry.platformio.org/libraries/marcoschwartz/LiquidCrystal_I2C

## Tools version

- PlatformIO Core, version 6.1.7
- Python 3.10

## License

[MIT](https://choosealicense.com/licenses/mit/)
