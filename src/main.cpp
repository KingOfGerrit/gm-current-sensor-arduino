#include <Arduino.h>

#include "PWM.hpp"
#include "Helpers.h"
#include "AverageCounter.hpp"

// Comment if LCD_I2C not used
//#define LCD_I2C

#ifdef LCD_I2C

#include "LiquidCrystal_I2C.h"

#include "LCDSpeedPrintValue.h"

#define lcd_address 0x27
#define lcd_cols 16
#define lcd_rows 2

LiquidCrystal_I2C lcd {lcd_address, lcd_cols, lcd_rows};

LCDSpeedPrintValue lcdAmps {0, 0, 4, "A:"};
LCDSpeedPrintValue lcdUs {lcd_cols - 7, 0, 4, "us:"};
LCDSpeedPrintValue lcdAverageLong {0, 1, 4, "Average:"};

#endif

AverageCounter<50> longAverage{};
AverageCounter<5> shortAverage{};

// In ms
#define pollingDelay 1000

// Should be a digital pin
#define gmSensorPin 2

// How to find zero value for the GM sensor:
// - Just connect the GM amp sensor to Arduino,
//   without a cable running through it, and read the 'μs' value from the serial port.
#define sensorZeroAmpsRef 4224
#define sensorMinimumRef 0

// How to calculate maximum and minimum mapping value for GM sensor:
// - With an increase in current by 1 amp, the GM amp sensor values
//   will be increased by 56 units (can be different on different versions/instances of sensor),
//   example: 0 amp = 4224, 1 amp = 4280, 3 amp = 4336
//   Lets call this value `oneAmpIncrement`
//
// - If we take for zero value from the sensor is 4224 units (just use `sensorZeroAmpsRef`), and the
//   max value from the sensor is 4224*2 = 8448 (assume that the max value is two times bigger than `sensorZeroAmpsRef`)
//   then we need to divide 4224 / 56 = 75.4 (56 is the `oneAmpIncrement`)
//   After dividing we receive a max mapping value that should be used
//   in the 'map' function in the last argument 'toHigh'.
//
// - Final formula:
//   ((sensorZeroAmpsRef * 2) - sensorZeroAmpsRef) / (oneAmpIncrement) = sensorMaxMappingValue
//
// - You should get a floating point value, remove the dot and put this value in `sensorMaxMappingValue`
//
// - More about the 'map' function can be found
//   by this link: https://www.arduino.cc/reference/en/language/functions/math/map/
#define sensorMaxMappingValue 754

PWM gmSensorPwm(gmSensorPin);
unsigned int gmSensorValue = 0;
float amps = 0;

void setup() {
    Serial.begin(115200);
    gmSensorPwm.begin(true);

#ifdef LCD_I2C
    lcd.init();      /*LCD display initialized*/
    lcd.clear();     /*Clear LCD display*/
    lcd.backlight(); /*Turn ON LCD Backlight*/

    lcdAmps.printLCDPrefix(lcd);
    lcdAverageLong.printLCDPrefix(lcd);
    lcdUs.printLCDPrefix(lcd);
#endif
}

void loop() {
    gmSensorValue = gmSensorPwm.getValue();

    amps = static_cast<float>(map(gmSensorValue, sensorMinimumRef, sensorZeroAmpsRef * 2,
                                  sensorMaxMappingValue * -1, sensorMaxMappingValue)) / 10.0f;

    longAverage.setValue(amps);
    shortAverage.setValue(amps);

    Serial.print("Amps = ");
    Serial.println(amps);
    Serial.print("On-Time μs = ");
    Serial.println(gmSensorValue);
    Serial.print("Average value long = ");
    Serial.println(longAverage.getAverage());
    Serial.print("Average value short = ");
    Serial.println(shortAverage.getAverage());

#ifdef LCD_I2C
    lcdAmps.updateValue(String(amps, 1));
    lcdAmps.printLCD(lcd);

    lcdUs.updateValue(String(gmSensorValue));
    lcdUs.printLCD(lcd);

    lcdAverageLong.updateValue(String(longAverage.getAverage(), 1));
    lcdAverageLong.printLCD(lcd);
#endif

    delay(pollingDelay);

    ++longAverage;
    ++shortAverage;
}
