//
// Created by toor on 8/16/23.
//
#include "Helpers.h"

#include <math.h>

long getFirstNumber(const long &number) {
    return number / 10;
}

long getLastNumber(const long &number) {
    return number % 10;
}

float roundToDp(float inValue, const int &decimalPlace) {
    float multiplier = powf( 10.0f, decimalPlace );
    inValue = roundf( inValue * multiplier ) / multiplier;
    return inValue;
}

