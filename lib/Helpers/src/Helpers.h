//
// Created by toor on 8/16/23.
//

#ifndef GM_CURRENT_SENSOR_ARDUINO_HELPERS_H
#define GM_CURRENT_SENSOR_ARDUINO_HELPERS_H

long getFirstNumber(const long &number);
long getLastNumber(const long &number);
float roundToDp(float inValue, const int &decimalPlace);

#endif //GM_CURRENT_SENSOR_ARDUINO_HELPERS_H
