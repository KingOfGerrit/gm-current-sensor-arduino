//
// Created by toor on 8/28/23.
//

#include "LCDSpeedPrintValue.h"

#include "LiquidCrystal_I2C.h"

LCDSpeedPrintValue::LCDSpeedPrintValue(const uint8_t &col, const uint8_t &row,
                                       const uint8_t &maxLengthOfValue)
        : m_col(col), m_row(row) {
    m_clearMsg = new char[maxLengthOfValue];

    for (uint8_t i = 0; i < maxLengthOfValue; i++) {
        m_clearMsg[i] = ' ';
    }
}

LCDSpeedPrintValue::LCDSpeedPrintValue(const uint8_t &col, const uint8_t &row,
                                       const uint8_t &maxLengthOfValue, const String &prefix)
        : LCDSpeedPrintValue(col, row, maxLengthOfValue) {
    m_prefixLength = prefix.length();
    m_prefix = new char[m_prefixLength];

    strcpy(m_prefix, prefix.c_str());
}

LCDSpeedPrintValue::~LCDSpeedPrintValue() {
    delete[] m_clearMsg;
    delete[] m_prefix;
}

void LCDSpeedPrintValue::printLCDPrefix(LiquidCrystal_I2C &lcd) {
    lcd.setCursor(m_col, m_row);
    lcd.print(m_prefix);
}

void LCDSpeedPrintValue::updateValue(const String &value) {
    m_value = value;

    uint8_t t = value.length();
    if (t > m_lengthOfValue || t < m_lengthOfValue) {
        m_isLengthChanged = true;
    }
    m_lengthOfValue = t;
}

void LCDSpeedPrintValue::printLCD(LiquidCrystal_I2C &lcd) {
    if (m_isLengthChanged) {
        lcd.setCursor(m_col + m_prefixLength, m_row);
        lcd.print(m_clearMsg);

        m_isLengthChanged = false;
    }

    lcd.setCursor(m_col + m_prefixLength, m_row);
    lcd.print(m_value);
}

String LCDSpeedPrintValue::getValue() const {
    return m_value;
}
