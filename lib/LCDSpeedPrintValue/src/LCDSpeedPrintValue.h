//
// Created by toor on 8/28/23.
//

#ifndef GM_CURRENT_SENSOR_ARDUINO_LCDSPEEDPRINTVALUE_H
#define GM_CURRENT_SENSOR_ARDUINO_LCDSPEEDPRINTVALUE_H

#include <Arduino.h>

class LiquidCrystal_I2C;

class LCDSpeedPrintValue {
public:
    LCDSpeedPrintValue(const uint8_t &col, const uint8_t &row, const uint8_t &maxLengthOfValue);
    LCDSpeedPrintValue(const uint8_t &col, const uint8_t &row, const uint8_t &maxLengthOfValue, const String &prefix);
    ~LCDSpeedPrintValue();

    LCDSpeedPrintValue() = delete;
    LCDSpeedPrintValue(const LCDSpeedPrintValue&) = delete;
    LCDSpeedPrintValue(LCDSpeedPrintValue&&) = delete;
    LCDSpeedPrintValue& operator=(const LCDSpeedPrintValue&) = delete;
    LCDSpeedPrintValue& operator=(LCDSpeedPrintValue&&) = delete;

    void printLCDPrefix(LiquidCrystal_I2C &lcd);
    void updateValue(const String &value);
    void printLCD(LiquidCrystal_I2C &lcd);

    String getValue() const;

private:
    String m_value {};

    uint8_t m_col = 0;
    uint8_t m_row = 0;

    uint8_t m_prefixLength = 0;
    bool m_isLengthChanged = false;
    uint8_t m_lengthOfValue = 0;

    char *m_prefix = nullptr;
    char *m_clearMsg = nullptr;
    // TODO
    //String m_prefix = String();
    //String m_clearMsg = String();
};

#endif //GM_CURRENT_SENSOR_ARDUINO_LCDSPEEDPRINTVALUE_H
