//
// Created by toor on 8/28/23.
//

#ifndef GM_CURRENT_SENSOR_ARDUINO_AVERAGECOUNTER_HPP
#define GM_CURRENT_SENSOR_ARDUINO_AVERAGECOUNTER_HPP

#include <Arduino.h>

template<uint8_t ARRAY_SIZE>
class AverageCounter {
public:
    explicit AverageCounter(const float &initValue = 0.1f);

    AverageCounter operator++();
    void setValue(const float &value);
    float getAverage();

    const float *getArray() const;
    byte getCurrentIndex() const;
    byte getArraySize() const;

private:
    bool m_isAverageChanged = false;
    byte m_currentIndex = 0;
    float m_savedAverage = 0.0f;
    float m_averageArray[ARRAY_SIZE] {};
};

// TODO
template<uint8_t ARRAY_SIZE>
AverageCounter<ARRAY_SIZE>::AverageCounter(const float &initValue /*= 0.1f*/) {
    for (auto &i : m_averageArray) {
        i = initValue;
    }

    m_isAverageChanged = true;
}

template<uint8_t ARRAY_SIZE>
AverageCounter<ARRAY_SIZE> AverageCounter<ARRAY_SIZE>::operator++() {
    m_isAverageChanged = true;

    if (m_currentIndex >= ARRAY_SIZE - 1) {
        m_currentIndex = 0;
    } else {
        m_currentIndex++;
    }

    return *this;
}

template<uint8_t ARRAY_SIZE>
void AverageCounter<ARRAY_SIZE>::setValue(const float &value) {
    m_isAverageChanged = true;
    m_averageArray[m_currentIndex] = value;
}

template<uint8_t ARRAY_SIZE>
float AverageCounter<ARRAY_SIZE>::getAverage() {
    if (!m_isAverageChanged) {
        return m_savedAverage;
    }

    float sum = 0.0f;
    for (const auto &i : m_averageArray) {
        sum += i;
    }

    m_savedAverage = sum / ARRAY_SIZE;
    return m_savedAverage;
}

template<uint8_t ARRAY_SIZE>
const float *AverageCounter<ARRAY_SIZE>::getArray() const {
    return m_averageArray;
}

template<uint8_t ARRAY_SIZE>
byte AverageCounter<ARRAY_SIZE>::getCurrentIndex() const {
    return m_currentIndex;
}

template<uint8_t ARRAY_SIZE>
byte AverageCounter<ARRAY_SIZE>::getArraySize() const {
    return ARRAY_SIZE;
}

#endif //GM_CURRENT_SENSOR_ARDUINO_AVERAGECOUNTER_HPP
