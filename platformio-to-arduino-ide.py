import os
import shutil
import warnings

Import("env")

class LibMissingSrcDir(UserWarning):
    pass

arduino_ide_directory_name = "arduino-ide"
arduino_ide_directory_path = f'./{arduino_ide_directory_name}'
project_name = os.path.basename(os.getcwd())

print(f'------------- Start running "platformio-to-arduino-ide.py" script -------------')
is_exist = os.path.exists(arduino_ide_directory_path)
if not is_exist:

    # Create a new directory because it does not exist
    os.makedirs(arduino_ide_directory_path)
    print(f"The '{os.path.basename(arduino_ide_directory_path)}' directory is created in '{os.getcwd()}'")
else:
    print(f"The '{os.path.basename(arduino_ide_directory_path)}' directory is exist in '{os.getcwd()}'")
    shutil.rmtree(arduino_ide_directory_path)
    print(f"The '{os.path.basename(arduino_ide_directory_path)}' "
          f"directory is removed and created again in '{os.getcwd()}'")

print('Collecting libraries')
root_dir = './lib'
for file in os.listdir(root_dir):
    d = os.path.join(root_dir, file)
    if os.path.isdir(d):
        print(f'Checking lib {d}')

        is_exist_src = os.path.exists(f'{d}/src')
        if is_exist_src:
            print(f'Copying src files from lib {d}')
            shutil.copytree(f'{d}/src', f'./{arduino_ide_directory_name}/{project_name}/', dirs_exist_ok=True)
        else:
            warnings.warn(f"Library `{d}` don't have `src` directory inside. "
                          f"Probably some libraries were not copied to Arduino IDE files.", LibMissingSrcDir)

print(f'Copying main.cpp')
shutil.copy2('./src/main.cpp', f'./{arduino_ide_directory_name}/{project_name}/{project_name}.ino')

print('------------- Done -------------')
